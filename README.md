# README

Hello, and welcome to the Alos Pre-PROcessing CHain (a.k.a. approach).

What is this repository for?
----------------------------

This is a repository to aid in the pre-processing of level 1.1 data from ALOS-1 and ALOS-2.

It was used by the New Partnerships for Sustainability (NEPSUS) project to assess the distribution of biomass and biomass change in Kilwa District, Tanzania.

Please do feel free to adapt these scripts for your own purposes. Determining the optimal processing chain for ALOS-1 and ALOS-2 data took me a long time, and I'd be very happy for others to pick up where I left off.

How does it work?
-----------------

Full documentation is hosted at:

http://approach.readthedocs.io

TL;DR
-----

This script is written in Python for use in Linux. You will need to have:

* A linux terminal
* The SNAP graph processing tool
* Python, with snappy installed.

To process all files in a directory of ALOS-1 or ALOS-2 files:

    python /path/to/approach/approach.py -o /path/to/output_dir/ -g /path/to/bin/gpt /path/to/ALOS_files*.zip

Is this the abolute best way to pre-process ALOS data?
------------------------------------------------------

Most likely not, but it's certainly the best I've managed.

Who do I contact?
-----------------

sam.bowers@ed.ac.uk
