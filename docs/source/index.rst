.. approach documentation master file, created by
   sphinx-quickstart on Mon Jan 15 09:37:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to approach's documentation!
====================================

The Alos Pre-PROcessing CHain (a.k.a. approach... sorry) is a repository to aid in the pre-processing of level 1.1 data from ALOS-1 and ALOS-2 with the SNAP graph processing tool.

Backscatter from the L-band ALOS-1 and ALOS-2 RADAR sensors is strongly associated with aboveground woody biomass in African woodlands and savannahs. This repository performs the pre-processing elements for producing similar maps. The gamma0 output geotiffs can be futher calibrated to measure aboveground biomass.

How do I get set up?
--------------------

This script is written in Python for use in Linux. You will need to have:

* A linux terminal
* Installed the SNAP graph processing tool
* Python, with snappy installed.

For more detailed instructions, see setup page.

Who do I talk to?
-----------------

sam.bowers@ed.ac.uk

Contents:
---------

.. toctree::
   :maxdepth: 2
   
   background.rst
   setup.rst
   preprocess.rst
   mosaic.rst
   under_the_bonnet.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

