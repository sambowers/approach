Setup instructions
==================

Requirements
------------

This toolset is written for use in Linux.

You will need access to a PC or server with at least:

* 32 GB of RAM to run SNAP.

For your images you might get away with as little as 8 GB, but in our experience 32 GB is required for reliable outputs.

Installing SNAP
---------------

SNAP is an ESA toolset used for (amongst other things) pre-processing data from Sentinel-1. SNAP for Linux can be downloaded from http://step.esa.int/main/download/.

To install SNAP, open a terminal window, change directory to the location you'd like to download SNAP (we recommend your home directory, at~/), and run the following commands:

.. code-block:: console

    wget http://step.esa.int/downloads/6.0/installers/esa-snap_sentinel_unix_6_0.sh
    bash esa-snap_sentinel_unix_6_0.sh
    
...and follow the instructions. *When prompted, be sure that you install the 'snappy' module for integration with python*.

.. note:: If using ALOS data from ESA, make sure that your version of SNAP is at least 6.0.

It's a good idea to increase the memory allocation to SNAP. This is controlled by the text file ``~/snap/bin/gpt.vmoptions``. This can be done with the following line:

.. code-block:: console
    
    echo '-Xmx32G' >> ~/snap/bin/gpt.vmoptions

For further details and up-to-date installation instructions, see the `SNAP website <http://step.esa.int/main/toolboxes/snap/>`_.

Installing snappy
-----------------

Snappy is a Python module that we used to extract metadata from ALOS datasets. It should work out-of-the-box following installation of SNAP, but in case not, here are some things to try:

* Find the directory that you istalled snap (default: ``~/snap``), and run ``./path/to/snap/bin/snappy-conf`` and follow the instructions it prints out.
* Make sure that Python can find the snappy package. You can do this by moving the snappy module to your Python ``site-packages`` directory.
* If that fails, try editing your ``~/.bashrc`` file to include the line ``export PYTHONPATH=$PYTHONPATH:/PATH/TO/snappy/``.
* If still in doubt, try consulting the `ESA STEP forum <http://forum.step.esa.int/>`_. 

Installing approach
-------------------

approach can be downloaded to a machine from its `repository <https://bitbucket.org/sambowers/approach>`_. To do this, open a terminal window and input:

.. code-block:: console

    git clone https://sambowers@bitbucket.org/sambowers/approach.git

Where do I get help?
--------------------

For help installing SNAP, it's best to refer to the `ESA STEP forum <http://forum.step.esa.int/>`_. For assistance in setting up and using approach, email `sam.bowers@ed.ac.uk <mailto:sam.bowers@ed.ac.uk>`_.
