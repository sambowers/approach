Preprocessing ALOS data on the command line
===========================================

Approach should be used on the Linux command line. Here the functionality of the command line tool is explained. In the next section we show how it works in more detail.

Showing help
------------

Help for ``preprocess.py`` can be viewed by typing ``python /path/to/approach/preprocess.py --help``:

.. code-block:: console
    
    usage: preprocess.py [-h] [-p HH/HV [HH/HV ...]] [-f] [-o PATH] [-g PATH]
                        [-n N_PROCESSES] [-v] [-e]
                        N [N ...]

    Perform pre-processing on ALOS-1 and ALOS-2 level 1.1 data to produce a
    terrain-corrected Gamma0 backscatter image.

    Required arguments:
    N                     Input files. Either specify a valid ALOS-1/2 input
                            file, or multiple files through wildcards.

    Optional arguments:
    -p HH/HV [HH/HV ...], --polarisation HH/HV [HH/HV ...]
                            Process either HH or HV polarisation. Defaults to
                            both.
    -f, --filter          Apply a refined lee filter to the processing chain.
    -o PATH, --output_dir PATH
                            Optionally specify an output directory. If nothing
                            specified, files will be output to current working
                            directory.
    -g PATH, --gpt PATH   This script uses the SNAP graph processing tool (gpt).
                            If not installed in your home directory, you'll need
                            to specify it's location. Defaults to ~/snap/bin/gpt.
    -n N_PROCESSES, --n_processes N_PROCESSES
                            Choose a number of paralell processes, defaults to 4.
    -v, --verbose         Like a chatty script? Use this flag.
    -e, --show_error      Print SNAP gpt output, including detailed error
                            messages.

An example run
--------------

Let's say we wanted to pre-process an ALOS-2 level 1.1 file from JAXA (``0000149017_001001_ALOS2117217000-160724.zip``), , considering only the HV polarisation. The command to submit would be:

.. code-block:: console

    python /path/to/approach/preprocess.py -p HV 0000149017_001001_ALOS2117217000-160724.zip
    
If we wanted to specify an output directory that is different to the present working directory, and to print script progress, the command might be:

.. code-block:: console

    python /path/to/approach/preprocess.py -p HV -o /path/to/output_directory/ -v 0000149017_001001_ALOS2117217000-160724.zip

To apply a speckle filter, use the ``-f`` flag:

.. code-block:: console

    python /path/to/approach/preprocess.py -p HV -f -o /path/to/output_directory/ -v 0000149017_001001_ALOS2117217000-160724.zip
    
If we had multiple files to process, we can use wildcards to select all the files we want to pre-process as follows:

.. code-block:: console

    python /path/to/approach/preprocess.py -p HV *ALOS*.zip

For some flavours of ALOS data (e.g. from ASF), you may need to unzip the directory for SNAP to understand the data. In this case, point SNAP to the *VOL* file.

.. note:: The SNAP graph processing tool can throw up a lot of errors. There shouldn't be much reason to be concerned by many of these, but do check that outputs look reasonable.

The script will take between 5 mins and 12 hours to run. The reasons for the disparity between tiles is still very unclear, but somehow relates to the underlying SNAP preprocessing chains.

Output
------

The script outputs geotiff files, and will process files separately for each polarisation.

Visualising in QGIS
-------------------

You should expect outputs that look a bit like this when viewed in a GIS:

.. image:: _static/approach_output.png
