Background
----------

.. image:: _static/alos_example_image.png

This repository contains scripts to perform pre-processing on level 1.1 data from the Phased Array L-band Synthetic Aperture Radar (PALSAR/PALSAR-2) instruments, which is part of Japan’s Advanced Land Observing Satellites (ALOS-1/ALOS-2). Radar satellite systems offer a number of advantages compared to traditional optical satellite systems (e.g. Landsat). For example, radar sensors are able to operate at any atmospheric conditions and equally either at day- and night-time. Furthermore, and most importantly in relation to forestry, long wavelength radar signal penetrates the canopy foliage and primarily interacts with forest aboveground biomass (i.e. tree trunks and stems), from which forest aboveground carbon can be calculated. The radar signal over forested areas that return to the sensor (backscatter) will be stronger compared to the backscatter measured in correspondence of a flat surface, as most of the signal bounces off in the direction opposite to the satellite. As a result of this, radar images will have brighter pixels in correspondence to forested areas, and darker pixels in correspondence to flat surfaces such as water bodies and bare soil. For these reasons radar can be considered a more suitable approach, compared to optical systems, to quantify forest carbon stocks, forest carbon change as well as forest carbon stock loss due to deforestation and forest degradation in tropical dry forests and African woodlands.

Further reading:

* Ryan, Casey M., et al. "Quantifying small‐scale deforestation and forest degradation in African woodlands using radar imagery." Global Change Biology 18.1 (2012): 243-257.
* Mitchard, Edward TA, et al. "Measuring biomass changes due to woody encroachment and deforestation/degradation in a forest–savanna boundary region of central Africa using multi-temporal L-band radar backscatter." Remote Sensing of Environment 115.11 (2011): 2861-2873.

