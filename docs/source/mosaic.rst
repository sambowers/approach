Mosaicking ALOS data on the command line
========================================

Approach also has a script to generate mosaics of ALOS data, which has a fairly small tile size. Data from radar instruments is often very nosiy, so averaging data from multiple scenes can also be beneficial.
Showing help
------------

Help for ``mosaic.py`` can be viewed by typing ``python /path/to/approach/mosaic.py --help``:

.. code-block:: console
    
    usage: mosaic.py [-h]
                    [-te TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT]
                    [-tr TARGET_RESOLUTION] [-e EPSG] [-o PATH] [-v] [-y YEAR]
                    N [N ...]

    Generate a mosaic of scenes from ALOS-1 or ALOS-2 imagery.

    Required arguments:
    N                     Input files. Either specify a valid ALOS-1/2 input
                            file, or multiple files through wildcards.
    -te TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT, --target_extent TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT TARGET_EXTENT
                            Specify image bounds as xmin ymin xmax ymax.
    -tr TARGET_RESOLUTION, --target_resolution TARGET_RESOLUTION
                            Specify an image resolution for the output image
    -e EPSG, --epsg EPSG  Specify the EPSG code of the output projection..

    Optional arguments:
    -o PATH, --output PATH
                            Optionally specify an output directory or file. If
                            nothing specified, we'll apply a standard filename and
                            output to the present working directory. Note: you
                            cannot process multiple input files with a single
                            output file name.
    -v, --verbose         Like a chatty script? Use this flag.
    -y YEAR, --year YEAR  Optionally specify a single year to process. If not
                            used, all years represented in infiles will be
                            processed.

An example run
--------------

Let's say we had processed two ALOS scenes, with filenames ``ALOS1_HV_20070702_ALPSRP076546990.tif`` and ``ALOS1_HV_20070702_ALPSRP076547000.tif``, and that we wanted to combine these two adjacent scenes into a single image. An example command to submit would be (given a known output extent):

.. code-block:: console

    python /path/to/approach/mosaic.py -te 1100000 8900000 1210000 9050000 -tr 25 -e 32736 ALOS1_HV_20070702_ALPSRP076546990.tif ALOS1_HV_20070702_ALPSRP076547000.tif
    
If we wanted to specify an output directory that is different to the present working directory, and to print script progress, the command might be:

.. code-block:: console

    python /path/to/approach/mosaic.py -te 1100000 8900000 1210000 9050000 -tr 25 -e 32736 -o /path/to/output_directory/ -v ALOS1_HV_20070702_ALPSRP076546990.tif ALOS1_HV_20070702_ALPSRP076547000.tif
    
If we had multiple files to process, we can use wildcards to select all the files we want to pre-process as follows:

.. code-block:: console

    python /path/to/approach/mosaic.py -te 1100000 8900000 1210000 9050000 -tr 25 -e 32736 ALOS1*.tif
    
If we had multiple files to process, but only wanted to process those from a single year:

    python /path/to/approach/mosaic.py -te 1100000 8900000 1210000 9050000 -tr 25 -e 32736 -y 2007 ALOS1*.tif

.. note:: Adjacent tracks of ALOS data are often poorly calibrated, the result of variability in the scene, particularly soil moisture. This package doesn't fix this, but consider histogram matching as a potential solution.

    
Output
------

The script outputs two geotiff files. The first (``g0_av*.tif``) records the mean gamma0 backscatter from all input scenes. The second (``nviews*.tif``) records the number of scenes contributing to each pixel in the output image.


Visualising in QGIS
-------------------

You should expect outputs that look a bit like this when viewed in a GIS:

.. image:: _static/approach_mosaic.png


