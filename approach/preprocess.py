#Samuel Bowers, University of Edinburgh, 2018

import argparse
import atexit
import functools
import glob
import os
import shutil
from snappy import jpy
from snappy import ProductIO
import time
import tempfile
import zipfile

import pdb

global to_delete
to_delete = []

def deleteTempFiles():
    '''
    Delete all files in global variable to_delete
    '''
    
    if len(to_delete) > 0:
        print('Deleting temporary files...')
        for temp_dir in to_delete:
            if os.path.exists(temp_dir):
                os.system('rm -rf %s'%temp_dir)
        


def unzipProduct(infile, temp_dir = None):
    '''
    Unzip an ALOS-1/2 file to temp_dir and return its 'VOL' file.
    
    Args:
        infile: ALOS-1/ALOS-2 input file in .zip format
    Returns:
        VOL file
    '''
    
    def unzip(infile, outpath):
        '''
        '''
        
        # Read .zip file
        zip_ref = zipfile.ZipFile(infile, 'r')
          
        # Extract zip file
        zip_ref.extractall(path = outpath)
        
        # Close zip file   
        zip_ref.close()
                
        return outpath      
    
    assert infile.endswith('.zip'), "Can only decompress a .zip file."
    
    print('  unzipping %s'%infile)
    
    # Create output file in temporary directory
    temp_dir = tempfile.mkdtemp(dir = temp_dir)
    
    # Remove these later
    to_delete.append(temp_dir)
    
    # Unzip file
    out_dir = unzip(infile, temp_dir)
    
    # Unzip interior .zip file, if present
    if len(glob.glob('%s/*.zip'%out_dir)) > 0:
        
        out_dir = unzip(glob.glob('%s/*.zip'%out_dir)[0], out_dir)
        
    # Return VOL file
    vol_file = glob.glob(out_dir + '/*VOL*') + glob.glob(out_dir + '/*/*VOL*')
    
    if len(vol_file) == 0: raise IOError("Unzipping failed to identify an ALOS-style file.")
    
    return vol_file[0]

def generate_outputname(infile, polarisation, lee_filter):
    '''
    Generates a standardised output filename
    
    Args:
        infile: ALOS-1/ALOS-2 input file, either in .zip format or pointed towards the '*VOL*' file.
        polarisation: Set to 'HH' or 'HV'.
        lee_filter: Set to True or False
    '''
    
    assert polarisation in ['HH', 'HV'], "Polarisation must be 'HH' or 'HV'."
    assert lee_filter in [True, False], "Lee filter must be True or False."
    
    # Open file
    product = ProductIO.readProduct(infile)
    
    # Get date
    datestring = product.getStartTime().toString()
    datelist = datestring.replace(' ','-').split('-')
    date_out = datelist[2] + str(time.strptime(datelist[1],'%b').tm_mon).zfill(2) + datelist[0]
    
    # Get sensor and level
    if product.getDescription()=='ALOS PALSAR product 1':
        sensor = 'ALOS1'
        level = '1-1'
    elif product.getDescription()=='ALOS PALSAR product 3':
        sensor = 'ALOS1'
        level = '1-5'
    elif product.getDescription()=='ALOS 2 product 1':
        sensor = 'ALOS2'
        level = '1-1'
    else:
        assert False, "This product's description doesn't match ALOS-1/ALOS-2 expectations. It purports to be: %s"%product.getDescription()    

    # Get scene code
    scene_out = product.getName().split('__')[-1]
    
    # Build filter string
    filter_string = 'filter' if lee_filter else 'nofilter'
    
    outputname = '%s_%s_%s_%s_%s_%s.tif'%(sensor,level,polarisation,filter_string,date_out,scene_out)
    
    # Close product and clear system
    product.dispose()
    System = jpy.get_type('java.lang.System')
    System.gc()
    
    return outputname



def select_graph(infile, lee_filter = False):
    '''
    Determines which xml file to use based on product details.
    
    Args:
        infile: ALOS-1/ALOS-2 input file, either in .zip format or pointed towards the '*VOL*' file.
        lee_filter: Set to True to output a filtered image. Defaults to False.
    '''
    
    # Open file
    product = ProductIO.readProduct(infile)
    
    # Get sensor
    if product.getDescription()=='ALOS PALSAR product 1':
        xmlfile = 'SNAP_chain%s_A1_1-1.xml'
    elif product.getDescription()=='ALOS PALSAR product 3':
        xmlfile = 'SNAP_chain%s_A1_1-5.xml'
    elif product.getDescription()=='ALOS 2 product 1':
        xmlfile = 'SNAP_chain%s_A2_1-1.xml'
    else:
        assert False, "This product's description doesn't match our ALOS-1/ALOS-2 expectations. It purports to be: %s"%product.getDescription()
        
    # Choose filter or non-filter option
    if lee_filter:
        xmlfile = xmlfile%'_filter'
    else:
        xmlfile = xmlfile%''
        
    # Get full path of xml file
    xmlpath = '%s/cfg/%s'%('/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[:-1]), xmlfile)
    
    # Close product and clear system
    product.dispose()
    System = jpy.get_type('java.lang.System')
    System.gc()
        
    return xmlpath


def process(infile, outfile, polarisation, xmlfile, gpt = '~/snap/bin/gpt', n_processes = 4, verbose = False, show_error = False):
    '''
    Executre SNAP on the command line with os.system
    
    Args:
        infile: ALOS-1/ALOS-2 input file, either in .zip format or pointed towards the '*VOL*' file.
        outfile: Output filename
        xmlfile: The SNAP batch processing file to run
        gpt: /path/to/gpt, the SNAP command line processing tool
        verbose: Be talkative
        show_error: Print SNAP gpt error messages
    '''
    
    assert type(n_processes) == int, "Number of processes must be an integer."
    
    error_flag = '-e ' if show_error else ''
    
    command = '%s %s -x %s-q %s -Pinputfile=%s -Poutputfile=%s -Ppolarisation=%s'%(gpt, xmlfile, error_flag, str(n_processes), infile, outfile, polarisation)
    
    if verbose: print('Doing %s'%command)
    
    os.system(command)



def main(infile, polarisation = ['HH', 'HV'], lee_filter = False, output_dir = os.getcwd(), gpt = '~/snap/bin/gpt', temp_dir = '/tmp', n_processes = 4, verbose = False, show_error = False):
    '''
    '''
    
    for pol in polarisation:
        assert pol in ['HH', 'HV'], "Polarisation can only be 'HH' or 'HV'." 
    
    start = time.time()
    
    if verbose: print('Processing %s:'%infile)
    
    for pol in polarisation:
        
        if verbose: print('  doing %s'%pol)
        
        try:
            
            # Read the 'VOL' or .zip file if possible
            product = ProductIO.readProduct(infile)
            
            if product is not None:
                # Continue if loading is successful
                infile_decompress = infile
                
                # Close product and clear system
                product.dispose()
                System = jpy.get_type('java.lang.System')
                System.gc()
    
            else:
                # Decompress is not
                infile_decompress = unzipProduct(infile, temp_dir = temp_dir)
                        
            # Build output name
            outfile = '%s/%s'%(output_dir, generate_outputname(infile_decompress, pol, lee_filter))
            
            # Don't reprocess
            if os.path.isfile(outfile):
                print('Output file for this image already exists. Skipping...')
                deleteTempFiles()
                continue
            
            # Select appropriate xml file for ALOS-1/ALOS-2 and options
            xmlfile = select_graph(infile_decompress, lee_filter = lee_filter)
                        
            # Execute Graph Processing Tool
            process(infile_decompress, outfile, pol, xmlfile, gpt = gpt, n_processes = n_processes, verbose = verbose, show_error = show_error)
            
            # Delete temp files
            deleteTempFiles()
            
        except Exception as e:
            print(e)
            raise
        

    
    end = time.time()
    
    if verbose: print('Processing finished after %s minutes!'% str(round((end - start)/60.,1)))
    

if __name__ == '__main__':
    '''
    ALOS pre-processing script for ALOS-1/ALOS-2 for AGB monitoring.
    '''
    
    # Set up command line parser
    parser = argparse.ArgumentParser(description = 'Perform pre-processing on ALOS-1 and ALOS-2 level 1.1 data to produce a terrain-corrected Gamma0 backscatter image.')
    
    parser._action_groups.pop()
    required = parser.add_argument_group('Required arguments')
    optional = parser.add_argument_group('Optional arguments')
    
    # Required arguments
    required.add_argument('infiles', metavar='N', type=str, nargs='+', help = 'Input files. Either specify a valid ALOS-1/2 input file, or multiple files through wildcards.')
    
    # Optional arguments
    optional.add_argument('-p', '--polarisation', type=str, nargs='+', metavar='HH/HV', default = ['HH','HV'], help = 'Process either HH or HV polarisation. Defaults to both.')
    optional.add_argument('-f', '--filter', action='store_true', default=False, help = 'Apply a refined lee filter to the processing chain.')
    optional.add_argument('-o', '--output_dir', type=str, metavar = 'PATH', default = os.getcwd(), help = "Optionally specify an output directory. If nothing specified, files will be output to current working directory.")
    optional.add_argument('-g', '--gpt', type=str, metavar = 'PATH', default = '~/snap/bin/gpt' , help = "This script uses the SNAP graph processing tool (gpt). If not installed in your home directory, you'll need to specify it's location. Defaults to ~/snap/bin/gpt.")
    optional.add_argument('-t', '--temp_dir', type=str, metavar = 'PATH', default = '/tmp' , help = "Directory to for storing temporary intermediate files. Defaults to '/tmp'.")
    optional.add_argument('-n', '--n_processes', type=int, default = 1, help = 'Choose a number of paralell processes, defaults to 1.')
    optional.add_argument('-v', '--verbose', action='store_true', default = False, help = 'Like a chatty script? Use this flag.')
    optional.add_argument('-e', '--show_error', action='store_true', default = False, help = 'Print SNAP gpt output, including detailed error messages.')
        
    # Get arguments   
    args = parser.parse_args()
    
    # Convert paths to absolute
    infiles = sorted([os.path.abspath(i) for i in args.infiles])
    output_dir = os.path.abspath(args.output_dir)
    temp_dir = os.path.abspath(args.temp_dir)
    
    # Test inputs are reasonable
    assert len(infiles)>0, "No input files found!"
    
    assert os.path.isdir(output_dir), "Output directory not found."
    assert os.path.isdir(temp_dir), "Temporary storage directory not found."
    
    # Make sure temp files deleted on exit
    atexit.register(deleteTempFiles)
    
    for infile in infiles:
        
        try:
            main(infile, polarisation = args.polarisation, lee_filter = args.filter, output_dir = output_dir, gpt = args.gpt, temp_dir = temp_dir, n_processes = args.n_processes, verbose = args.verbose, show_error = args.show_error)
        except KeyboardInterrupt:
            break
        except Exception as e:
            # Print error and continue to next file
            print(e)
            continue

