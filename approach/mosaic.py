
import argparse
import datetime as dt
import glob
import numpy as np
import os
import time

import pdb

def buildMetadataDictionary(extent_dest, res, EPSG):
    '''
    Build a metadata dictionary to describe the destination georeference info
    
    Args:
        extent_dest: List desciribing corner coordinate points in destination CRS [xmin, ymin, xmax, ymax]
        res: Integer describing pixel size 
        EPSG: EPSG code of destination coordinate reference system. Best a UTM projection. See: https://www.epsg-registry.org/ for codes.
    
    Returns:
        A dictionary containg projection info.
    '''
    
    from osgeo import osr
    
    # Set up an empty dictionary
    md = {}
    
    # Define projection from EPSG code
    md['EPSG_code'] = EPSG
    
    # Get GDAL projection string
    proj = osr.SpatialReference()
    proj.ImportFromEPSG(EPSG)
    md['proj'] = proj
    
    # Get image extent data
    md['ulx'] = float(extent_dest[0])
    md['lry'] = float(extent_dest[1])
    md['lrx'] = float(extent_dest[2])
    md['uly'] = float(extent_dest[3])
    md['xres'] = float(res)
    md['yres'] = float(-res)

    # Save current resolution for future reference
    md['res'] = res
    
    # Calculate array size
    md['nrows'] = int((md['lry'] - md['uly']) / md['yres'])
    md['ncols'] = int((md['lrx'] - md['ulx']) / md['xres'])
    
    # Define gdal geotransform (Affine)
    md['geo_t'] = (md['ulx'], md['xres'], 0, md['uly'], 0, md['yres'])
    
    return md


def buildOutArrays(md):
    '''
    '''
    
    g0_sum = np.zeros((md['nrows'], md['ncols']), dtype = np.float32)
    views = np.zeros((md['nrows'], md['ncols']), dtype = np.uint8)
    
    return g0_sum, views


def getALOSmetadata(infile):
    '''
    Uses level_1 file format output by preprocess.py
    '''
    
    from osgeo import gdal
    from osgeo import osr
    
    assert infile.split('.')[-1] == 'tif' or infile.split('.')[-1] == 'tiff', "Input data must be in geoTiff format"
    
    ds = gdal.Open(infile,0)
    geo_t = ds.GetGeoTransform()
    proj = ds.GetProjection()

    # Calculate extent
    ulx = geo_t[0]
    uly = geo_t[3]
    xres = geo_t[1]
    yres = geo_t[5]
    
    lrx = ulx + (ds.RasterXSize * xres)
    lry = uly + (ds.RasterYSize * yres)
    
    extent = [ulx, lry, lrx, uly]
    
    # Get EPSG
    srs = osr.SpatialReference(wkt=proj)
    srs.AutoIdentifyEPSG()
    epsg = int(srs.GetAttrValue("AUTHORITY", 1))
    
    # Extract date from filename
    datestring = infile.split('/')[-1].split('_')[4]
    date = dt.date(int(datestring[:4]), int(datestring[4:6]), int(datestring[6:]))
    
    return extent, xres, epsg, date
    

def getALOSData(infile, md_dest):
    '''
    '''
    
    from osgeo import gdal
    
    # Get source image projection info
    ds_source = gdal.Open(infile)
    proj_source = ds_source.GetProjection()
    
    # Set up output image
    driver = gdal.GetDriverByName('MEM')
    ds_dest = driver.Create('', md_dest['ncols'], md_dest['nrows'], 1, gdal.GDT_Float32)
    ds_dest.SetGeoTransform(md_dest['geo_t'])
    ds_dest.SetProjection(md_dest['proj'].ExportToWkt())
    
    # Reproject source to output CRS
    gdal.ReprojectImage(ds_source, ds_dest, proj_source, md_dest['proj'].ExportToWkt(), gdal.GRA_NearestNeighbour)
    
    return ds_dest.GetRasterBand(1).ReadAsArray()


def updateOutArrays(g0, g0_sum, views):
    '''
    '''
    
    g0_sum += g0
    views[g0 > 0.] = views[g0 > 0.] + 1
    
    return g0_sum, views


def outputData(g0_sum, views, md_dest, outloc = os.getcwd(), year = None):
    '''
    '''
    
    from osgeo import gdal
    
    g0_out = np.zeros_like(g0_sum, dtype = np.float32)
       
    # Calculate the mean average
    g0_out[views>0] = g0_sum[views>0] / views[views>0]
    
    if year == None: year = 'all'
    
    # Generate a filename
    g0_av_filename = '%s/g0_av_%s.tif'%(outloc, str(year))
    nviews_filename = '%s/nviews_%s.tif'%(outloc, str(year))
    
    # Output mean backscatter map
    drv = gdal.GetDriverByName('GTiff')
    dest = drv.Create(g0_av_filename, md_dest['ncols'], md_dest['nrows'], 1, gdal.GDT_Float32, options = ['COMPRESS=LZW'])
    dest.SetGeoTransform(md_dest['geo_t'])
    dest.SetProjection(md_dest['proj'].ExportToWkt())
    dest.GetRasterBand(1).WriteArray(g0_out)
    dest = None

    # Output number of views map
    drv = gdal.GetDriverByName('GTiff')
    dest = drv.Create(nviews_filename, md_dest['ncols'], md_dest['nrows'], 1, gdal.GDT_Byte, options = ['COMPRESS=LZW'])
    dest.SetGeoTransform(md_dest['geo_t'])
    dest.SetProjection(md_dest['proj'].ExportToWkt())
    dest.GetRasterBand(1).WriteArray(views)
    dest = None
        

def main(infiles, target_extent, target_resolution, epsg, outloc = os.getcwd(), year = None, verbose = False):
    '''
    '''
    
    # Get destination metadata into a nice format
    md_dest = buildMetadataDictionary(target_extent, target_resolution, epsg)
    
    # Generate output arrays
    g0_sum, views = buildOutArrays(md_dest)
    
    # For each input file
    for infile in infiles:
        
        if verbose: print('Processing %s'%infile)
        
        # Get source metadata
        source_extent, source_resolution, source_epsg, source_date = getALOSmetadata(infile)
        md_source = buildMetadataDictionary(source_extent, source_resolution, source_epsg)
        
        # Only process file if it meets year specification
        if year == None or source_date.year == year:
            
            # Load ALOS data in output projection
            g0 = getALOSData(infile, md_dest)
            
            # Update output arrays
            g0_sum, views = updateOutArrays(g0, g0_sum, views)
    
    # Output_data
    outputData(g0_sum, views, md_dest, outloc = outloc, year = year)
        
        
if __name__ == '__main__':



    # Set up command line parser
    parser = argparse.ArgumentParser(description = 'Generate a mosaic of scenes from ALOS-1 or ALOS-2 imagery.')
    
    parser._action_groups.pop()
    required = parser.add_argument_group('Required arguments')
    optional = parser.add_argument_group('Optional arguments')

    # Required arguments
    required.add_argument('infiles', metavar='N', type=str, nargs='+', help = 'Input files. Either specify a valid ALOS-1/2 input file, or multiple files through wildcards.')
    required.add_argument('-te', '--target_extent', nargs = 4, type=float, help = 'Specify image bounds as xmin ymin xmax ymax.')
    required.add_argument('-tr', '--target_resolution', type=float, help='Specify an image resolution for the output image')
    required.add_argument('-e', '--epsg', type=int, help='Specify the EPSG code of the output projection..')
    
    
    # Optional arguments
    optional.add_argument('-o', '--output', type=str, metavar = 'PATH', default = os.getcwd(), help = "Optionally specify an output directory or file. If nothing specified, we'll apply a standard filename and output to the present working directory. Note: you cannot process multiple input files with a single output file name.")
    optional.add_argument('-v', '--verbose', action='store_true', default = False, help = 'Like a chatty script? Use this flag.')
    optional.add_argument('-y', '--year', type=int, default = None, help='Optionally specify a single year to process. If not used, all years represented in infiles will be processed.')


    # Get arguments   
    args = parser.parse_args()

    # Convert paths to absolute
    infiles = sorted([os.path.abspath(i) for i in args.infiles])
    output = os.path.abspath(args.output)
    
    # Test inputs are reasonable
    if len(infiles)>1:
        assert os.path.isdir(output), "Where multiple input files specified, you cannot specify a single output file. Try a directory instead."

    # If an output file is specified, test that its a geotiff
    if os.path.isfile(output):
        assert output.split('.')[-1] in ['tif','tiff'], 'Output must be a geotiff (.tif/.tiff) or a directory'


    main(infiles, args.target_extent, args.target_resolution, args.epsg, outloc = output, year = args.year, verbose = args.verbose)



